<?php

/**
 * Perform a service attachments sync: get all attachments for the next batch of nodes.
 */
function service_attachments_sync() {
  $tasks = service_attachments_get_tasks_batch(variable_get('service_attachments_tasks_per_run', 10));
  foreach ($tasks as $task) {
    service_attachments_sync_task($task['nid'], $task['service']);
  }
}

/**
 * Calculate the next batch of tasks (node, service) that must be synced.
 *
 * @param $batch_size
 *   The size of the batch (maximum number of node ids that can be returned).
 * @return
 *   An array of tasks. Each task is an associative array with a 'nid' and a 'service' element.
 */
function service_attachments_get_tasks_batch($batch_size) {
  $services = service_attachments_get_services();
  $types = service_attachments_get_node_types('info');

  $nids = array();

  if (count($types) > 0) {
    // try to find a node where a sync hasn't happened yet for a certain service
    $sql_parts = array();
    $sql_part_template = "SELECT n.nid, '%s' as service, n.created FROM {node} n WHERE n.type = '%s' AND n.nid NOT IN (SELECT nid FROM {service_attachments_info} WHERE service = '%s')";

    foreach ($types as $type => $info) {
      foreach ($info['services'] as $service) {
        $sql_parts[] = sprintf($sql_part_template, $service, $type, $service);
      }
    }

    if (count($sql_parts) > 0) {
      $sql = "SELECT c.nid, c.service FROM (". implode(' UNION ', $sql_parts) .") AS c %s c.created DESC";

      // if statistics and content view counting is enabled, we order by view count first; so first popular nodes, then newly created nodes
      if (module_exists('statistics') && variable_get('statistics_count_content_views', 0)) {
        $sql = sprintf($sql, 'LEFT JOIN {node_counter} nc ON nc.nid = c.nid ORDER BY nc.daycount DESC, nc.totalcount DESC, ');
      }
      else {
        $sql = sprintf($sql, 'ORDER BY');
      }

      $result = db_query_range($sql, 0, $batch_size);

      while ($row = db_fetch_object($result)) {
        $nids[] = array('nid' => $row->nid, 'service' => $row->service);
      }

      // if everything has already been synced once, we do a new sync if the attachment is too old
      if (count($nids) == 0 && variable_get('service_attachments_resync_min_age', -1) > -1) {
        $sql_parts = array();
        foreach ($types as $type => $info) {
          $sql_parts[] = "(n.type = '". $type ."' AND sa.service IN ('". implode("', '", $info['services']) ."'))";
        }

        $sql = "SELECT sa.nid, sa.service, (sa.changed - sa.created) AS diff FROM {service_attachments_info} sa INNER JOIN {node} n ON n.nid = sa.nid WHERE (UNIX_TIMESTAMP(NOW()) - sa.created) > %d AND (". implode(' OR ', $sql_parts)  .") ORDER BY diff ASC, n.created ASC";

        $result = db_query_range($sql, variable_get('service_attachments_resync_min_age', -1), 0, $batch_size);

        while ($row = db_fetch_object($result)) {
          $nids[] = array('nid' => $row->nid, 'service' => $row->service);
        }
      }
    }
  }
  return $nids;
}

/**
 * Perform a sync for a certain task.
 *
 * @param $nid
 *   The node id for which to find attachments.
 * @param $service
 *   The service for which to find attachments.
 */
function service_attachments_sync_task($nid, $service) {
  $node = node_load($nid, NULL, TRUE);

  $candidates = service_attachments_get_candidates($node, $service);

  if (count($candidates) > 0) {
    $matches_info = service_attachments_find_matches($candidates, $service);
    $candidate = $matches_info['candidate'];
    $matches = $matches_info['matches'];
  }
  else {
    $candidate = '';
    $matches = array();
  }

  db_query("DELETE FROM {service_attachments_attachments} WHERE nid = %d AND service = '%s'", $nid, $service);

  $sql = "UPDATE {service_attachments_info} SET changed = %d, query = '%s' WHERE nid = %d AND service = '%s'";
  db_query($sql, $_SERVER['REQUEST_TIME'], $candidate, $nid, $service);

  if (!db_affected_rows()) {
    $sql = "INSERT INTO {service_attachments_info} (nid, service, created, changed, query) VALUES (%d, '%s', %d, %d, '%s')";
    db_query($sql, $nid, $service, $_SERVER['REQUEST_TIME'], $_SERVER['REQUEST_TIME'], $candidate);
  }

  foreach ($matches as $i => $match) {
    $sql = "INSERT INTO {service_attachments_attachments} (nid, service, title, url_detail, resource, status, position) VALUES (%d, '%s', '%s', '%s', '%s', %d, %d)";
    db_query($sql, $nid, $service, $match['title'], $match['url'], $match['resource'], 1, $i);
  }

  watchdog('service_attach', 'Succesfully processed !title for service !service. '. $message, array('!title' => $node->title, '!service' => $service));
}

/**
 * Try to find a match for a certain service for a certain set of candidates.
 *
 * @param $candidates
 *   An array of strings representing the candidates.
 * @param $service
 *   A string representing the service for which attachments should be found.
 * @return
 *   An associative array (candidate, title, resource, url) representing a match if one is found. An empty array if no match was found.
 */
function service_attachments_find_matches($candidates, $service) {
  $matches_info = array('candidate' => '', 'matches' => array());
  $services = service_attachments_get_services();

  foreach ($candidates as $candidate) {
    $cid = $service .':'. variable_get('service_attachments_service_'. $service .'_limit', 10) .':'. md5($candidate);
    if (service_attachments_cache_check_enabled() && $cache = cache_get($cid, 'cache_service_attachments')) {
      $matches = $cache->data;
    }
    else {
      service_attachments_load_includes();
      $matches = call_user_func($services[$service]['callback'], $candidate, variable_get('service_attachments_service_'. $service .'_limit', 10));
    }
    if (service_attachments_cache_check_enabled()) {
      cache_set($cid, $matches, 'cache_service_attachments', service_attachments_cache_expires());
    }

    if (count($matches) > 0) {
      $matches_info['candidate'] = $candidate;
      $matches_info['matches'] = $matches;
      break;
    }
  }

  return $matches_info;
}

/**
 * Get a list of candidates for a certain node and service.
 *
 * @param $node
 *   A node object.
 * @param $service
 *   A string representing the service.
 * @return
 *   An array of candidates.
 */
function service_attachments_get_candidates($node, $service) {
  ob_start();
  $code = variable_get('service_attachments_candidates_'. $node->type, '');
  $candidates = array();
  $result = eval($code);
  if (is_array($result)) {
    $candidates = $result;
  }
  ob_end_clean();

  // remove blacklisted candidates
  if (count($candidates) > 0) {
    $placeholders = db_placeholders($candidates, 'varchar');
    $result = db_query("SELECT candidate FROM {service_attachments_blacklist} WHERE candidate IN ($placeholders)", $candidates);
    $blacklist = array();
    while ($row = db_fetch_object($result)) {
      $blacklist[] = $row->candidate;
    }
    $candidates = array_diff($candidates, $blacklist);
  }

  return $candidates;
}

/**
 * Load all service include files.
 */
function service_attachments_load_includes() {
  static $loaded = FALSE;
  if ($loaded) return;

  // loading include files
  $services = service_attachments_get_services();
  foreach ($services as $service => $info) {
    if (isset($info['file']) && !empty($info['file'])) {
      module_load_include('inc', $info['module'], $info['file']);
    }
  }

  $loaded = TRUE;
}

/**
 * Calculate cache entry expire time.
 *
 * @return
 *   CACHE_TEMPORARY if no minimum cache life time is set, $_SERVER['REQUEST_TIME'] + minimum cache lifetime if it is.
 */
function service_attachments_cache_expires() {
  $expire = CACHE_TEMPORARY;
  if (variable_get('service_attachments_cache_lifetime', 0) > 0) {
    $expire = $_SERVER['REQUEST_TIME'] + variable_get('service_attachments_cache_lifetime', 0);
  }
  return $expire;
}

/**
 * Check if Service attachments caching is enabled.
 *
 * @return
 *   One of the two constants (SERVICE_ATTACHMENTS_CACHE_DISABLED, SERVICE_ATTACHMENTS_CACHE_ENABLED), stating if caching is disabled or enabled.
 */
function service_attachments_cache_check_enabled() {
  return variable_get('service_attachments_cache_status', SERVICE_ATTACHMENTS_CACHE_DISABLED) == SERVICE_ATTACHMENTS_CACHE_ENABLED;
}
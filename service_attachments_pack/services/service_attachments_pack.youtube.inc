<?php

/**
 * Callback for the Youtube service for Service attachments service.
 *
 * @return
 *   An array containing matches.
 */
function service_attachments_pack_youtube_get($query, $limit) {
  $matches = array();

  // set feed URL
  $feed_url = 'http://gdata.youtube.com/feeds/api/videos?q='. urlencode($query) .'&start-index=1&max-results='. ($limit > 5 ? $limit : 5);

  // read feed into SimpleXML object
  $sxml = simplexml_load_file($feed_url);

  if ($sxml) {
    $counts = $sxml->children('http://a9.com/-/spec/opensearchrss/1.0/');
    $total = $counts->totalResults;

    foreach ($sxml->entry as $entry) {
      $media = $entry->children('http://search.yahoo.com/mrss/');

      foreach ($media->group->content as $content) {
        $attrs = $content->attributes();
        if ($attrs['type'] == 'application/x-shockwave-flash') {
          $media_content_type = 'application/x-shockwave-flash';
          $media_content_url = strval($attrs['url']);
        }
      }

      if ($media_content_url) {

        // get the published date
        $date = $entry->published;
        $date = strtotime($date);
        $date = date('j-m-Y', $date);

        // get video player URL
        $attrs = $media->group->player->attributes();
        $watch = strval($attrs['url']);

        // get video thumbnail
        $attrs = $media->group->thumbnail[0]->attributes();
        $thumbnail = strval($attrs['url']);

        // get <yt:duration> node for video length
        $yt = $media->children('http://gdata.youtube.com/schemas/2007');
        $attrs = $yt->duration->attributes();
        $length = intval($attrs['seconds']);

        $embed = '';
        $embed .= '<object width="425" height="350">';
        $embed .= '<param name="movie" value="'.  $media_content_url .'"></param>';
        $embed .= '<embed src="'.  $media_content_url .'"';
        $embed .= 'type="'.  $media_content_type .'" width="425" height="350">';
        $embed .= '</embed>';
        $embed .= '</object>';

        $matches[] = array(
          'url' => $watch,
          'title' => strval($media->group->title),
          /* 'thumb' => $thumbnail,
          'date' => $date,
          'desc' => strval($media->group->description),
          'duration' => sprintf('%0.2f', $length/60),*/
          'resource' => $embed
        );
        
        if (count($matches) == $limit) break;
      }
    }
  }
  return $matches;
}
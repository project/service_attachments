<?php

/**
 * Callback for the Flickr service for Service attachments service.
 *
 * @return
 *   An array containing matches.
 */
function service_attachments_pack_flickr_get($query, $limit) {
  $matches = array();

  $secret = service_attachments_variable_get('flickr', 'secret');
  $api_key = service_attachments_variable_get('flickr', 'apikey');

  $args['api_key'] = $api_key;
  $args['format'] = 'php_serial';
  $args['method'] = 'flickr.photos.search';
  $args['page'] = '1';
  $args['per_page'] = strval($limit);
  $args['tag_mode'] = 'any';
  $args['text'] = $query;

  // Build an argument hash API signing
  $arg_hash = '';
  foreach ($args as $k => $v) {
    $arg_hash .= $k . $v;
  }

  $args['api_sig'] = md5($secret . $arg_hash);

  foreach ($args as $k => $v) {
    $encoded_params[] = urlencode($k) .'='. urlencode($v);
  }

  $url = 'http://api.flickr.com/services/rest/?'. implode('&', $encoded_params);

  $result = drupal_http_request($url);

  $photos = unserialize($result->data);

  if ($photos['photos']['total'] > 0) {
    foreach ($photos['photos']['photo'] as $photo) {
      $matches[] = array(
        'title' => $photo['title'],
        'resource' => service_attachments_pack_flickr_photo_img($photo),
        'url' => "http://flickr.com/photos/". $photo['owner'] ."/". $photo['id'],
      );
    }
  }
  return $matches;
}

function service_attachments_pack_flickr_photo_img($photo, $size = NULL, $format = NULL) {
  // early images don't have a farm setting so default to 1.
  $farm = isset($photo['farm']) ? $photo['farm'] : 1;
  $server = $photo['server'];
  // photoset's use primary instead of id to specify the image.
  $id = isset($photo['primary']) ? $photo['primary'] : $photo['id'];
  $secret = $photo['secret'];

  return "http://farm{$farm}.static.flickr.com/{$server}/{$id}_{$secret}". ($size ? "_$size." : '.') . ($size == 'o' ? $format : 'jpg');
}
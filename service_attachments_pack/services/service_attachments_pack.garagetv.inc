<?php

/**
 * Callback for the GarageTV service for Service attachments service.
 *
 * @return
 *   An array containing matches.
 */
function service_attachments_pack_garagetv_get($query) {
  $matches = array();

  $key = service_attachments_variable_get('garagetv', 'apikey');
  $url = 'http://www.garagetv.com/api.ashx?apikey='. $key .'&method=SearchVideos&keyword='. urlencode($query) .'&pagesize='. $limit;

  $xml = simplexml_load_file($url);

  if ($xml) {
    $xml_attrs = $xml->attributes;
    if (intval($xml_attrs['error']) == 0) {
      foreach ($xml->videos[0]->videoinfo as $video) {
        $matches[] = array(
          'title' => strval($video->name[0]),
          'resource' => strval($video->embedcode[0]),
          'url' => strval($video->viewer[0]),
        );
      }
    }
  }

  return $matches;
}
<?php

/**
 * Callback for the Wikipedia NL service for Service attachments service.
 *
 * @return
 *   An array containing matches.
 */
function service_attachments_pack_wikipedia_nl_get($query, $limit) {
  return _service_attachments_pack_wikipedia_get($query, $limit, 'nl');
}

/**
 * Callback for the Wikipedia EN service for Service attachments service.
 *
 * @return
 *   An array containing matches.
 */
function service_attachments_pack_wikipedia_en_get($query, $limit) {
  return _service_attachments_pack_wikipedia_get($query, $limit, 'en');
}

/**
 * Helper function for the Wikipedia services for Service attachments service.
 *
 * @return
 *   An array containing matches.
 */
function _service_attachments_pack_wikipedia_get($query, $limit, $language = 'nl') {
  $matches = array();

  // wikipedia doesn't support "" queries
  $query = str_replace('"', '', $query);

  $url = 'http://'. $language .'.wikipedia.org/w/api.php?format=xml&action=opensearch&search='. urlencode($query) .'&limit='. $limit;

  ini_set('user_agent', 'Drupal (+http://drupal.org/');
  
  $xml = simplexml_load_file($url, 'SimpleXMLElement');

  if ($xml) {
    foreach ($xml->Section[0]->Item as $item) {
      $matches[]  = array(
        'title' => strval($item->Text[0]),
        'url' => strval($item->Url[0]),
        'resource' => '',
      );
    }
  }

  return $matches;
}
<?php

/**
 * Service attachments module settings form.
 *
 * @see system_settings_form()
 */
function service_attachments_settings() {
  $form['config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Configuration'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['config']['service_attachments_tasks_per_run'] = array(
    '#type' => 'select',
    '#title' => t('Tasks per cron run'),
    '#description' => t('The maximum number of sync tasks per cron run. A task is a combination of a node and a service.'),
    '#options' => drupal_map_assoc(array(5, 10, 15, 20, 25, 50, 100)),
    '#default_value' => variable_get('service_attachments_tasks_per_run', 10),
  );
  $hour = 60 * 60;
  $period = drupal_map_assoc(array($hour, 2 * $hour, 3 * $hour, 6 * $hour, 12 * $hour, 24 * $hour), 'format_interval');
  $period[-1] = t('Never (disable)');
  $period[0] = t('Every cron run');
  ksort($period);
  $form['config']['service_attachments_sync_frequency'] = array(
    '#type' => 'select',
    '#title' => t('Sync frequency'),
    '#multiple' => FALSE,
    '#description' => t('When should the synchronisation happen?'),
    '#options' => $period,
    '#default_value' => variable_get('service_attachments_sync_frequency', -1),
  );

  $day = 60 * 60 * 24;
  $period = drupal_map_assoc(array($day, 7 * $day, 14 * $day, 28 * $day, 52 * 7 * $day), 'format_interval');
  $period[-1] = t('Never');
  ksort($period);
  $form['config']['service_attachments_resync_min_age'] = array(
    '#type' => 'select',
    '#title' => t('Minimum age before resync'),
    '#description' => t('How old should attachments be before they come available for resync?'),
    '#options' => $period,
    '#default_value' => variable_get('service_attachments_resync_min_age', -1),
  );

  return system_settings_form($form);
}

/**
 * Service attachments services settings form.
 *
 * @see system_settings_form()
 */
function service_attachments_settings_services() {
  $services = service_attachments_get_services(FALSE);
  foreach ($services as $service => $info) {
    $prefix = 'service_attachments_service_'. $service;
    $form[$prefix] = array(
      '#type' => 'fieldset',
      '#title' => $info['name'],
      '#collapsible' => TRUE,
      '#collapsed' => !variable_get($prefix .'_enable', '0'),
      '#description' => isset($info['description']) && !empty($info['description']) ? $info['description'] : FALSE,
    );
    $form[$prefix][$prefix .'_enable'] = array(
      '#type' => 'radios',
      '#title' => t('Status'),
      '#description' => t('Disabling this service will keep the attachments already gathered for this service but will hide the service from all display and administration screens.'),
      '#default_value' => variable_get($prefix .'_enable', '0'),
      '#options' => array(
        0 => t('Disabled'),
        1 => t('Enabled')
      ),
    );
    $form[$prefix][$prefix .'_limit'] = array(
      '#type' => 'select',
      '#title' => t('Limit'),
      '#description' => t('Maximum number of results to fetch per node / service.'),
      '#default_value' => variable_get($prefix .'_limit', 10),
      '#options' => drupal_map_assoc(array(1, 5, 10, 15, 20, 25, 50, 100)),
    );

    service_attachments_settings_services_settings_form($form, $info['settings'], $prefix);
  }
  return system_settings_form($form);
}

/**
 * Helper function for service attachments services settings form. Adds the subform for service settings to the main form.
 */
function service_attachments_settings_services_settings_form(&$form, $settings, $prefix) {
  foreach ($settings as $setting => $info) {
    $form[$prefix][$prefix .'_setting_'. $setting] = array(
      '#type' => 'textfield',
      '#title' => $info['name'],
      '#description' => $info['description'],
      '#default_value' => variable_get($prefix .'_setting_'. $setting, ''),
    );
  }
}

/**
 * Service attachments overview of blacklisted candidates.
 */
function service_attachments_settings_blacklist() {
  $header = array(t('Candidate'), t('Operations'));

  $rows = array();

  $sql = "SELECT bid, candidate FROM {service_attachments_blacklist}";
  $result = pager_query($sql, 50);

  while ($row = db_fetch_object($result)) {
    $rows[] = array(
      $row->candidate,
      l(t('Delete'), 'admin/settings/service_attachments/blacklist/delete/'. $row->bid)
    );
  }

  if (empty($rows)) {
    $rows[] = array(array('data' => t('No candidates blacklisted yet.'), 'colspan' => '2', 'class' => 'message'));
  }

  return theme('table', $header, $rows) . theme('pager');
}

/**
 * Service attachments blacklist form to add candidates to the list.
 */
function service_attachments_settings_blacklist_add_form() {
  $form = array();

  $form['candidate'] = array(
    '#type' => 'textfield',
    '#title' => t('Candidate'),
    '#description' => t('The title you enter here appears on the page.'),
    '#required' => TRUE,
    '#size' => 40,
    '#maxlength' => 512,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add to blacklist'),
  );

  return $form;
}

/**
 * Validation handler for the Service attachments blacklist form to add candidates to the list.
 */
function service_attachments_settings_blacklist_add_form_validate($form, &$form_state) {
  $count = db_result(db_query("SELECT count(bid) FROM {service_attachments_blacklist} WHERE candidate = '%s'", $form_state['values']['candidate']));
  if ($count > 0) {
    form_set_error('candidate', t('This candidate is already on the blacklist.'));
  }
}

/**
 * Submit handler for the Service attachments blacklist form to add candidates to the list.
 */
function service_attachments_settings_blacklist_add_form_submit($form, &$form_state) {
  db_query("INSERT INTO {service_attachments_blacklist} (candidate) VALUES ('%s')", $form_state['values']['candidate']);
  $form_state['redirect'] = 'admin/settings/service_attachments/blacklist';
}

/**
 * Menu callback -- ask for confirmation of blacklist candidate deletion
 */
function service_attachments_settings_blacklist_delete_confirm(&$form_state, $bid) {
  $candidate = db_fetch_object(db_query("SELECT bid, candidate FROM {service_attachments_blacklist} WHERE bid = %d", $bid));

  $form['bid'] = array(
    '#type' => 'value',
    '#value' => $bid,
  );

  return confirm_form($form,
    t('Are you sure you want to delete %name?', array('%name' => $candidate->candidate)),
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/settings/service_attachments/blacklist',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Execute blacklist candidate deletion after confirmation
 */
function service_attachments_settings_blacklist_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    db_query("DELETE FROM {service_attachments_blacklist} WHERE bid = %d", $form_state['values']['bid']);
  }

  $form_state['redirect'] = 'admin/settings/service_attachments/blacklist';
}

/**
 * Menu callback. Get an overview of all service attachments.
 */
function service_attachments_overview() {
  $header = array(
    array('data' => t('Service'), 'field' => 'sai.service'),
    array('data' => t('Node type'), 'field' => 'n.type'),
    array('data' => t('Title')),
    array('data' => t('Query')),
    array('data' => t('Status'), 'field' => 'saa.status'),
    array('data' => t('Age'), 'field' => 'sai.changed', 'sort' => 'desc'),
    array('data' => t('Node')),
  );

  $types = service_attachments_get_node_types('info');

  if (count($types) > 0) {
    $sql = "SELECT sai.nid, n.type as node_type, sai.service, saa.title, saa.url_detail, sai.query, saa.status, sai.changed FROM {service_attachments_info} sai INNER JOIN {service_attachments_attachments} saa ON saa.nid = sai.nid AND saa.service = sai.service INNER JOIN {node} n ON n.nid = sai.nid WHERE sai.query <> ''";

    // we only want enabled types and services
    $sql_parts = array();
    foreach ($types as $type => $info) {
      $sql_parts[] = "(n.type = '". $type ."' AND sai.service IN ('". implode("', '", $info['services']) ."'))";
    }

    $sql .= " AND (". implode(' OR ', $sql_parts) .")";

    $result = pager_query($sql . tablesort_sql($header), 50);

    $rows = array();
    while ($row = db_fetch_object($result)) {
      $rows[] = array(
        $row->service,
        $row->node_type,
        !empty($row->url_detail) ? l($row->title, $row->url_detail) : $row->title, $row->query,
        $row->status ? t('Active') : t('Inactive'),
        format_interval($_SERVER['REQUEST_TIME'] - $row->changed),
        l(t('View'), 'node/'. $row->nid)
      );
    }
  }

  if (empty($rows)) {
    $rows[] = array(array('data' => t('No attachments available.'), 'colspan' => '8', 'class' => 'message'));
  }

  return theme('table', $header, $rows) . theme('pager');
}
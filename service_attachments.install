<?php

/**
 * Implementation of hook_uninstall().
 */
function service_attachments_install() {
  drupal_install_schema('service_attachments');

  $link = array('!link' => l(t('Administer > Site configuration > Service attachments'), 'admin/settings/service_attachments'));
  drupal_set_message(t("Service attachments module settings are available under !link", $link));
}

/**
 * Implementation of hook_uninstall().
 */
function service_attachments_uninstall() {
  drupal_uninstall_schema('service_attachments');

  db_query("DELETE FROM {variable} WHERE name LIKE 'service_attachments_%'");
  cache_clear_all('variables', 'cache');
}

/**
 * Implementation of hook_schema().
 */
function service_attachments_schema() {
  $schema['service_attachments_info'] = array(
    'description' => t("Stores the service attachments to nodes."),
    'fields' => array(
      'nid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t("The node's {node}.nid."),
      ),
      'service' => array(
        'type' => 'varchar',
        'default' => '',
        'length' => 32,
        'not null' => TRUE,
        'description' => t('Service user for generating the attachment.'),
      ),
      'created' => array(
        'type' => 'int',
        'not null' => FALSE,
        'description' => t('UNIX timestamp when this attachment was created.'),
      ),
      'changed' => array(
        'type' => 'int',
        'not null' => FALSE,
        'description' => t('UNIX timestamp when this attachment was last updated (saved).'),
      ),
      'query' => array(
        'type' => 'varchar',
        'default' => '',
        'length' => 512,
        'not null' => TRUE,
        'description' => t('The query that was sent to the service to retrieve the resulting attachment.'),
      ),
    ),
    'primary key' => array('nid', 'service'),
    'indexes' => array(
       'nid' => array('nid'),
       'service' => array('service'),
    ),
  );
  $schema['service_attachments_attachments'] = array(
    'description' => t("Stores the service attachments to nodes."),
    'fields' => array(
      'nid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t("The node's {node}.nid."),
      ),
      'service' => array(
        'type' => 'varchar',
        'default' => '',
        'length' => 32,
        'not null' => TRUE,
        'description' => t('Service user for generating the attachment.'),
      ),
      'title' => array(
        'type' => 'varchar',
        'default' => '',
        'length' => 255,
        'not null' => TRUE,
        'description' => t('Title of the attachment.'),
      ),
      'url_detail' => array(
        'type' => 'varchar',
        'default' => '',
        'length' => 255,
        'not null' => TRUE,
        'description' => t('URL of the attachment.'),
      ),
      'resource' => array(
        'description' => t('Resource of the attachment (embed, image url, ...).'),
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'big'
      ),
      'status' => array(
        'description' => t('Boolean indicating whether the attachment is published.'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1
      ),
      'position' => array(
        'description' => t('Position indicating the quality of the match.'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0
      ),
    ),
    'indexes' => array(
       'nid' => array('nid'),
       'service' => array('service'),
       'position' => array('position'),
    ),
  );
  $schema['service_attachments_blacklist'] = array(
    'description' => t("Stores the service attachments blacklisted candidates."),
    'fields' => array(
      'bid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('The primary identifier for a blacklisted candidate.'),
      ),
      'candidate' => array(
        'type' => 'varchar',
        'default' => '',
        'length' => 512,
        'not null' => TRUE,
        'description' => t('Blacklisted candidate.'),
      ),
    ),
    'primary key' => array('bid'),
    'indexes' => array(
       'candidate' => array('candidate'),
    ),
  );
  $schema['cache_service_attachments'] = array(
    'description' => t('Cache table for the Service attachment service requests.'),
    'fields' => array(
      'cid' => array(
        'description' => t('Primary Key: Unique cache ID.'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'data' => array(
        'description' => t('A collection of data to cache.'),
        'type' => 'blob',
        'not null' => FALSE,
        'size' => 'big'
      ),
      'expire' => array(
        'description' => t('A Unix timestamp indicating when the cache entry should expire, or 0 for never.'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0
      ),
      'created' => array(
        'description' => t('A Unix timestamp indicating when the cache entry was created.'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0
      ),
      'headers' => array(
        'description' => t('Any custom HTTP headers to be added to cached data.'),
        'type' => 'text',
        'not null' => FALSE
      ),
      'serialized' => array(
        'description' => t('A flag to indicate whether content is serialized (1) or not (0).'),
        'type' => 'int',
        'size' => 'small',
        'not null' => TRUE,
        'default' => 0
      )
    ),
    'indexes' => array('expire' => array('expire')),
    'primary key' => array('cid'),
  );

  return $schema;
}

/**
 * Implementation of hook_requirements().
 */
function service_attachments_requirements($phase) {
  $requirements = array();

  $t = get_t();

  if ($phase == 'runtime') {
    // Raise warning if minimum cache lifetime is set higher than minimum age before resync.
    if (variable_get('service_attachments_cache_status', SERVICE_ATTACHMENTS_CACHE_DISABLED) == SERVICE_ATTACHMENTS_CACHE_ENABLED && variable_get('service_attachments_resync_min_age', -1) != -1 && variable_get('service_attachments_cache_lifetime', 0) > variable_get('service_attachments_resync_min_age', -1)) {
      $requirements['service_attachments_cache_time'] = array(
        'title' => $t('Service attachments cache time'),
        'description' => $t('Your cache time for Service attachments is set higher than the minimum age before resync. This way the first resync attempt will be redundant.  Please specify a lower value at @link.', array('@link' => l('the performance settings page', 'admin/settings/performance'))),
        'severity' => REQUIREMENT_ERROR,
        'value' => $t('Not configured'),
      );
    }
  }

  return $requirements;
}
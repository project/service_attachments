<?php

/**
 * Implementation of hook_form().
 */
function service_attachments_node_attachments_form($form_state, $node) {
  $form = array();

  $form['#tree'] = TRUE;

  $form['nid'] = array(
    '#type' => 'hidden',
    '#value' => $node->nid
  );

  $types = service_attachments_get_node_types('info');

  // Module is diabled for this content type or no services are enabled for it.
  if (!isset($types[$node->type]) || count($types[$node->type]['services']) == 0) {
    $form['no_services'] = array(
      '#value' => t('Service attachments is disabled for the content type (%type) of this node or no service has been enabled.', array('%type' => $node->type)),
      '#prefix' => '<div>',
      '#suffix' => '</div>',
    );
  }
  // Service attachments is enabled for this content type.
  else {
    $form['service_attachments'] = array(
      '#type' => 'fieldset',
      '#title' => t('Find matches'),
      '#description' => t('Find matches for this node.'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );

    $form['service_attachments']['find'] = array(
      '#type' => 'submit',
      '#value' => t('Find'),
      '#submit' => array('service_attachments_node_attachments_find_submit'),
    );

    /*$result = db_query("SELECT * FROM {service_attachments} WHERE nid = %d AND title <> ''", $node->nid);
    $attachments = array();
    while ($row = db_fetch_object($result)) {
      $attachments[$row->service] = $row;
    }*/

    $services = service_attachments_get_services();

    foreach ($types[$node->type]['services'] as $service) {
      $info = $services[$service];
      $form['services'][$service] = array(
        '#type' => 'fieldset',
        '#title' => $info['name'],
        '#collapsible' => TRUE,
        '#collapsed' => !isset($node->service_attachments[$service]),
      );

      if (!isset($node->service_attachments[$service])) {
        $form['services'][$service]['attachment']['title'] = array(
          '#value' => t('No attachment found (yet) for this service.'),
          '#prefix' => '<div>',
          '#suffix' => '</div>',
        );
      }
      else {
        foreach ($node->service_attachments[$service] as $i => $attach) {
          $form['services'][$service]['attachment'][$attach->position] = array(
            '#type' => 'fieldset',
            '#title' => !empty($attach->title) ? $attach->title : t('No title available'),
            '#collapsible' => TRUE,
            '#collapsed' => TRUE,
          );

          if (!empty($attach->resource)) {
            $form['services'][$service]['attachment'][$attach->position]['resource'] = array(
              '#title' => t('Resource'),
              '#type' => 'item',
              '#value' => $attach->resource,
              '#description' => t('The resource of the resulting attachment.'),
            );
          }
          $form['services'][$service]['attachment'][$attach->position]['title'] = array(
            '#title' => t('Title'),
            '#type' => 'item',
            '#value' => l($attach->title, $attach->url_detail),
            '#description' => t('The title of the resulting attachment.'),
          );

          if (!empty($info['url_more'])) {
            $form['services'][$service]['attachment'][$attach->position]['url_more'] = array(
              '#title' => t('URL for more items'),
              '#type' => 'item',
              '#value' => l($attach->query, sprintf($info['url_more'], urlencode($attach->query))),
              '#description' => t('The URL to the page on the service where more attachment can be found.'),
            );
          }
          else {
            $form['services'][$service]['attachment'][$attach->position]['query'] = array(
              '#title' => t('Query'),
              '#type' => 'item',
              '#value' => $attach->query,
              '#description' => t('The query that was issued to find the resulting attachment.'),
            );
          }

          $form['services'][$service]['attachment'][$attach->position]['status'] = array(
            '#type' => 'checkbox',
            '#title' => t('This is a valid attachment'),
            '#default_value' => $attach->status,
            '#description' => t('Uncheck this checkbox if you want to mark the found result as invalid.')
          );
        }
      }
    }
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
  }

  return $form;
}

/**
 * Submit handler for the node service attachments form.
 */
function service_attachments_node_attachments_form_submit($form, &$form_state) {
  foreach ($form_state['values']['services'] as $service => $attachments_info) {
    foreach ($attachments_info['attachment'] as $position => $attachment) {
      db_query("UPDATE {service_attachments_attachments} SET status = %d WHERE nid = %d AND service = '%s' AND position = %d", $attachment['status'], $form_state['values']['nid'], $service, $position);
    }
  }
  drupal_set_message(t('Succesfully changed the state of the attachments.'));
}

/**
 * Submit handler for the 'Find' button on the node service attachments form. Tries to find attachments for a node.
 */
function service_attachments_node_attachments_find_submit($form, &$form_state) {
  $matches = array();

  module_load_include('inc', 'service_attachments', 'service_attachments.sync');

  $types = service_attachments_get_node_types('info');

  $node = menu_get_object();

  foreach ($types[$node->type]['services'] as $service) {
    service_attachments_sync_task($node->nid, $service);
  }

  drupal_set_message(t('Succesfully synced @services services.', array('@services' => count($types[$node->type]['services']))));
}